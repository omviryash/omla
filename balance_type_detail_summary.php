<?php
if(isset($_POST["submitBtn"]) && $_POST["cmbSummary"]=='detail')
{
	$postUserId = $_POST["user_id"];
	$btype = $_POST["btype"];
	if($postUserId=="")
	{
		if($_SESSION['usertype'] == 2)
			$qrySelUser = "SELECT * FROM users WHERE usertype=1 AND user_id IN(SELECT retailerID FROM brokerwise_retailer WHERE brokerID=".$_SESSION['user_id'].") ORDER BY username";
		else
			$qrySelUser = "SELECT * FROM users WHERE 1 ORDER BY user_id ASC";
	}
	else
	{
		$qrySelUser = "SELECT * FROM users WHERE 1 AND user_id = '".$postUserId."' ORDER BY user_id ASC";
	}
	$resSelUser = mysql_query($qrySelUser);
	?>
	<table id="example1" cellpadding="4" cellspacing="0" border="1" class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>User</th>
				<?php
				if($btype==1)
				{
					?>
					<th>Opening</th>
					<?php
				}
				?>
				<th style="padding: 0px;border: 0px;">
					<table id="example1" cellpadding="4" cellspacing="0" border="1" class="table table-bordered table-striped" width="100%">
						<tr>
							<th style="width: 100px;">Date</th>
							<?php
							if($btype==1 || $btype==3)
							{
								?>
								<th style="width: 80px;">Credit</th>
								<th style="width: 80px;">Debit</th>
								<?php
							}
							if($btype==1 || $btype==2)
							{
								?>
								<th style="width: 80px;">Bought</th>
								<th style="width: 80px;">Comm.</th>
								<th style="width: 80px;">Win</th>
								<?php
							}
							if($btype==1)
							{
								?>
								<th style="width: 90px;">Balance</th>
								<?php
							}
							else	// for type 2 and 3
							{
								?>
								<th style="width: 80px;">Difference</th>
								<?php
							}
							?>
						</tr>
					</table>
				</th>
			</tr>
		</thead>
		<tbody>
	<?php
	if(mysql_num_rows($resSelUser)>0)
	{
		while($qFetch = mysql_fetch_array($resSelUser))
		{
			$retailerId = $qFetch["user_id"];
			$totalCommision = 0;
			$userName = $qFetch["username"];
			$creditAmount = $debitAmount = $totalBought = 0;
			
			if($btype==1)
			{
				//----- code for opening balance ----
				$previousDate = date('Y-m-d', strtotime($_POST['from_date'] .' -1 day'));
				$qrySelOpeningBalance = "SELECT SUM( quantity * product_price ) as totalBought , 
										DATE( receipt_master.receipt_time ) as receiptDate, receipt_master.retailer_id, SUM( (quantity * product_price) * user_commission /100 ) AS totalCommission FROM receipt_master LEFT JOIN receipt_details ON receipt_master.receipt_id = receipt_details.receipt_id WHERE receipt_master.receipt_cancel=0 and receipt_master.retailer_id='".$retailerId."' AND DATE( receipt_master.receipt_time ) <= '".$previousDate."'
										GROUP BY receipt_master.retailer_id ASC";
				$resSelOpeningBalance = mysql_query($qrySelOpeningBalance);
				$numRowsOpeningBalance = mysql_num_rows($resSelOpeningBalance);
				$openingBalance = 0;
				if($numRowsOpeningBalance>0)
				{
					$qFetchOpeningBalance = mysql_fetch_array($resSelOpeningBalance);
					$qrySelTrans1 = "SELECT * FROM transaction WHERE retailer_id='".$retailerId."' AND DATE(transaction_time) <= '".$previousDate."'";
					$resSelTrans1 = mysql_query($qrySelTrans1);
					$creditAmount1 = $debitAmount1 = 0;
					if(mysql_num_rows($resSelTrans1)>0)
					{
						while($qFetchTrans1 = mysql_fetch_array($resSelTrans1))
						{
							if($qFetchTrans1["cr_dr"]=='Credit')
							{
								$creditAmount1 += $qFetchTrans1['amount'];
							}
							if($qFetchTrans1["cr_dr"]=='Debit')
							{
								$debitAmount1 += $qFetchTrans1['amount'];
							}
						}
					}
					$totalCommision1 = $qFetchOpeningBalance["totalCommission"];
					$totalBought1 = $qFetchOpeningBalance["totalBought"];
					
					$win1 = 0;
					$sSQL = "SELECT receipt_master.receipt_id,receipt_master.receipt_scan,receipt_master.receipt_cancel,draw.win_product_id,draw.win_amount,draw.is_jackpot,draw.win_product_id2,draw.win_amount2,draw.is_jackpot2 FROM receipt_master 
										LEFT JOIN draw ON receipt_master.draw_id = draw.draw_id 
										WHERE receipt_master.receipt_cancel=0 and receipt_master.retailer_id='".$retailerId."' AND DATE( draw.drawdatetime ) <= '".$previousDate."'";
					$rs = mysql_query($sSQL);
					if(mysql_num_rows($rs) > 0){
						while($row = mysql_fetch_array($rs)){
							$sSQL = "SELECT * FROM receipt_details WHERE receipt_id = ".$row["receipt_id"];
							$rs2 = mysql_query($sSQL);
							if(mysql_num_rows($rs2) > 0){
								while($row2 = mysql_fetch_array($rs2)){
									if($row["receipt_scan"] == "1" && $row["receipt_cancel"] != "1"){
										if($row2["product_id"] == $row["win_product_id"] && $row["win_amount"] > 0)
											$win1 = $win1 + ($row2["quantity"]*$row["win_amount"]);
										if($allow_twowin && $row['win_product_id2'] > 0 && $row2["product_id"] == $row["win_product_id2"])
											$win1 = $win1 + ($row2["quantity"]*$row["win_amount2"]);
									}
								}
							}
						}
					}
					
					
					//echo '<br>===>'.$creditAmount1 .'-'. $debitAmount1 .'-'. $totalBought1 .'+'. $totalCommision1 .'+'. $win1;
					$openingBalance = $creditAmount1 - $debitAmount1 - $totalBought1 + $totalCommision1 + $win1;
				}
				//------------------------------------
			}
			$transaction = array();
			?>
			<tr>
				<td><?php echo $userName; ?></td>
				<?php
				if($btype==1)
				{
					?>
					<td style="vertical-align: top;"><?php echo $openingBalance; ?></td>
					<?php
				}
				?>
				<td style="padding: 0px;border: 0px;">
					<table id="example1" cellpadding="4" cellspacing="0" border="1" class="table table-bordered table-striped" width="100%">
						<?php
						$qrySelTrans = "SELECT * FROM transaction WHERE retailer_id='".$retailerId."' AND DATE(transaction_time) BETWEEN '".$_POST['from_date']."' AND '".$_POST['to_date']."'";
						$resSelTrans = mysql_query($qrySelTrans);
						//$creditAmount = $debitAmount = 0;
						$calBalance = 0;
						if($btype==1 || $btype==3)
						{
							if(isset($openingBalance)) {
								$calBalance = $openingBalance;
							}
							if(mysql_num_rows($resSelTrans)>0)
							{
								while($qFetchTrans = mysql_fetch_array($resSelTrans))
								{
									if($qFetchTrans["cr_dr"]=='Credit')
									{
									$calBalance += $qFetchTrans['amount'];
										//$creditAmount += $qFetchTrans['amount'];
										?>
										<tr>
											<td style="width: 100px;"><?php echo date('d/m/Y',strtotime($qFetchTrans["transaction_time"])); ?></td>
											<td align="right" style="width: 80px;"><?php echo $qFetchTrans['amount']; ?></td>
											<td style="width: 80px;">&nbsp;</td>
											<?php
											if($btype==1)
											{
												?>
												<td style="width: 80px;">&nbsp;</td>
												<td style="width: 80px;">&nbsp;</td>
												<td style="width: 80px;">&nbsp;</td>
												<?php
											}
											?>
											<td style="width: 80px;"><?php echo $calBalance; ?></td>
										</tr>
										<?php
										
									}
									if($qFetchTrans["cr_dr"]=='Debit')
									{
										//$debitAmount += $qFetchTrans['amount'];
										$calBalance -= $qFetchTrans['amount'];
										?>
										<tr>
											<td style="width: 100px;"><?php echo date('d/m/Y',strtotime($qFetchTrans["transaction_time"])); ?></td>
											<td align="right" style="width: 80px;">&nbsp;</td>
											<td style="width: 80px;"><?php echo $qFetchTrans['amount']; ?></td>
											<?php
											if($btype==1)
											{
												?>
												<td style="width: 80px;">&nbsp;</td>
												<td style="width: 80px;">&nbsp;</td>
												<td style="width: 80px;">&nbsp;</td>
												<?php
											}
											?>
											<td><?php echo $calBalance; ?></td>
										</tr>
										<?php
										
									}
								}
							}
						}
						
						$qrySelHalfSummary = "SELECT SUM( quantity * product_price ) AS perRecordBought,
											DATE(receipt_master.receipt_time ) AS receiptDate, receipt_master.retailer_id, 
												SUM((quantity * product_price) * user_commission /100 ) AS totalCommission,receipt_master.receipt_id
												FROM receipt_master
												LEFT JOIN receipt_details ON receipt_master.receipt_id = 
												receipt_details.receipt_id
												WHERE receipt_master.receipt_cancel=0 and  receipt_master.retailer_id='".$retailerId."' AND DATE( receipt_master.receipt_time )
												BETWEEN '".$_POST['from_date']."' AND '".$_POST['to_date']."'
												GROUP BY receipt_master.receipt_id ASC ORDER BY DATE(receipt_master.receipt_time) ASC";
						$resSelHalfSummary	= mysql_query($qrySelHalfSummary);
						if(mysql_num_rows($resSelHalfSummary)>0)
						{
							while($qFetchHalfSummary = mysql_fetch_array($resSelHalfSummary))
							{
								$win = 0;
								$sSQL = "SELECT receipt_master.receipt_id,receipt_master.receipt_scan,receipt_master.receipt_cancel,draw.win_product_id,draw.win_amount,draw.is_jackpot,draw.win_product_id2,draw.win_amount2,draw.is_jackpot2 FROM receipt_master 
									LEFT JOIN draw ON receipt_master.draw_id = draw.draw_id 
									WHERE receipt_master.receipt_cancel=0 and  receipt_master.retailer_id='".$retailerId."' AND receipt_master.receipt_id='".$qFetchHalfSummary["receipt_id"]."' AND DATE( draw.drawdatetime ) BETWEEN '".$_POST['from_date']."' AND '".$_POST['to_date']."'";
								$rs = mysql_query($sSQL);
								if(mysql_num_rows($rs) > 0){
									while($row = mysql_fetch_array($rs)){
										$sSQL = "SELECT * FROM receipt_details WHERE receipt_id = ".$row["receipt_id"];
										$rs2 = mysql_query($sSQL);
										if(mysql_num_rows($rs2) > 0){
											while($row2 = mysql_fetch_array($rs2)){
												if($row["receipt_scan"] == "1" && $row["receipt_cancel"] != "1"){
													if($row2["product_id"] == $row["win_product_id"] && $row["win_amount"] > 0)
														$win = $win + ($row2["quantity"]*$row["win_amount"]);
													if($allow_twowin && $row['win_product_id2'] > 0 && $row2["product_id"] == $row["win_product_id2"])
														$win = $win + ($row2["quantity"]*$row["win_amount2"]);
												}
											}
										}
									}
								}
								
								
								$totalCommision = $qFetchHalfSummary["totalCommission"];
								$totalBought = $qFetchHalfSummary["perRecordBought"];
								$calBalance = $calBalance - ($totalBought) + ($totalCommision + $win);
								?>
								<?php
								if($btype==1)
								{
									?>
									<tr>
										<td style="width: 100px;"><?php echo date('d/m/Y',strtotime($qFetchHalfSummary["receiptDate"])); ?></td>
										<td align="right" style="width: 80px;">&nbsp;</td>
										<td style="width: 80px;">&nbsp;</td>
										<td style="width: 80px;"><?php echo $totalBought; ?></td>
										<td style="width: 80px;"><?php echo formatAmt($totalCommision); ?></td>
										<td style="width: 80px;"><?php echo round($win); ?></td>
										<td style="width: 80px;"><?php echo round($calBalance); ?></td>
									</tr>
									<?php
								}
								if($btype==2)
								{
									?>
									<tr>
										<td style="width: 100px;"><?php echo date('d/m/Y',strtotime($qFetchHalfSummary["receiptDate"])); ?></td>
										<td style="width: 80px;"><?php echo $totalBought; ?></td>
										<td style="width: 80px;"><?php echo formatAmt($totalCommision); ?></td>
										<td style="width: 80px;"><?php echo round($win); ?></td>
										<td style="width: 80px;"><?php echo round($calBalance); ?></td>
									</tr>
									<?php
								}
								?>
								
								<?php
							}
						}
						?>
					</table>
				</td>
			</tr>
			<?php
		}
		
	}
	else
	{
		?>
		<tr>
			<th colspan="10">No Users Found</th>
		</tr>
		<?php
	}
	?>
	</tbody>
			<!-- <tfoot>
				<tr>                                                
					<th colspan="2" align="right">Total</th>
					<th colspan="3"></th>
				</tr>
			</tfoot> -->
		</table>
	<?php
}
?>
