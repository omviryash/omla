<?php
include_once('includes/basepath.php');

if($_SESSION['usertype'] !=2)
{	
	header('Location: index.php');
}
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/<?php echo $cssLoad;?>.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/cycle.js"></script>
	<script type="text/javascript" src="js/cycle.tile.js"></script>
		
</head>


<body>
	<div id="centerPopup"></div>
	<?php if($package == 2){ ?>
	<div class="top">
	<?php include_once('menuBroker.php');?>
		<b><span style="float:right;">Hello,<?php if(isset($_SESSION['username'])) echo $_SESSION['username'];?></span></b>
		<div class="clearfix"></div>
		<span style="float:right;"><a href="logout.php">Logout</a></span>
		<div class="clearfix"></div>		
	</div>
	
	<?php }else{ ?>
	<div id="mainWrapper">		
	<?php include_once('menuBroker.php');?>
			<div id="Righttop">
				<strong>Hello,<?php echo $_SESSION['username'];?></strong><br />
				<a href="logout.php">Logout</a><br />			
			</div>	
	</div>
	<?php } ?>	
 
</body>
</html>
