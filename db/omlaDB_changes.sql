ALTER TABLE `users` CHANGE `usertype` `usertype` INT(11) NOT NULL COMMENT '0=admin,1=retailer, 2=broker, 99=superadmin';


INSERT INTO `users` (`user_id`, `username`, `password`, `current_balance`, `usertype`, `user_commission`, `user_ip`, `is_active`, `online`, `city`, `note`) VALUES (NULL, 'superadmin', '6d69625180ad7731eef7fbde0523c35f', '0', '99', '0', NULL, '1', NULL, 'Rajkot', 'Test');


CREATE TABLE IF NOT EXISTS `brokerwise_retailer` (
`bID` int(10) unsigned NOT NULL,
  `brokerID` int(10) unsigned NOT NULL,
  `retailerID` int(10) unsigned NOT NULL,
  `assignDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE  `brokerwise_retailer` ADD PRIMARY KEY (  `bID` ) ;

ALTER TABLE  `brokerwise_retailer` CHANGE  `bID`  `bID` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ;

ALTER TABLE `transaction` ADD `fromBroker` INT UNSIGNED NOT NULL AFTER `retailer_id`;