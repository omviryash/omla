<nav id="menu" class="menu">
  <ul>
      <li><a href="dashboardBroker.php" target="_blank">Dashboard</a></li>
      <li class="has-submenu">
        <a href="#">Entry</a>
        <ul>
          <li><a href="transactionBroker.php?cr_dr=Credit">Add</a></li>
          <li><a href="transactionBroker.php?cr_dr=Debit">Less</a></li>
        </ul>
      </li>
      <li class="has-submenu">
        <a href="#">User Reports</a>
        <ul>
          <li><a href="summaryOfBalanceBroker.php">Balance Summary</a></li>
          <li><a href="report2Broker.php">Scan Report</a></li>
          <li><a href="balance_typeBroker.php?btype=1">Type 1</a></li>
          <li><a href="balance_typeBroker.php?btype=3">Type 3</a></li>
        </ul>
      </li>      
      <li><a href="logout.php">Logout</a></li>
      <?php
      $selBrokBal = 'SELECT current_balance, user_id, usertype FROM users WHERE user_id='.$_SESSION['user_id'].' AND usertype=2';
      $resBrokBal = mysql_query($selBrokBal);
      $rowBrokBal = mysql_fetch_array($resBrokBal);
      ?>
      <li><span style="color:darkblue;font-weight:bold;font-size:14px;">Your Current Balance: <?php echo $rowBrokBal['current_balance']; ?></span></li>
  </ul>
</nav>
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery-ui.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css" /> 
<script type="text/javascript" src="js/tinynav.js"></script>
<script>
$(function () {
  $("#menu").tinyNav();
});
</script>