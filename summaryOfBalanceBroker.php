<?php include_once('includes/basepath.php'); 
if($_SESSION['usertype'] != 2){
	header('Location: index.php');
}

unset($_SESSION['msg']);
?>

<html>
    <head> 
        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <link rel="stylesheet" href="css/jquery-ui.css" /> 
        <script src="js/jquery-ui.js"></script>  

        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <script language="javascript">
            jQuery(document).ready(function() {
                jQuery('#date').datepicker({ dateFormat: 'dd-mm-yy' });
            })
        </script>
    </head>
    <body>
        <div class="top">
            <?php include_once('menuBroker.php'); ?>
            <b><span style="float:right;">Hello,<?php if (isset($_SESSION['username'])) echo $_SESSION['username']; ?></span></b>
            <div class="clearfix"></div><br/>
            <span style="float:right;"><a href="logout.php">Logout</a></span>
            <div class="clearfix"></div>		
        </div>	
        <div style="margin-left:140px">
        <form method="POST" id="frmdata">
        	<b>Balance Summary - Filter</b> <select name="filter" onchange="frmdata.submit();">
        		<option value="username" <?php if(isset($_POST["filter"]) && $_POST["filter"] == "username") echo 'selected'; ?>>Username</option>
        		<option value="city,username" <?php if(isset($_POST["filter"]) && $_POST["filter"] == "city,username") echo 'selected'; ?>>City</option>
        		</select>
        </form>
      	</div>
            <?php
			  if(isset($_GET['pass']) && $_GET['pass'] == "Ok")
			    echo "Change password successfull";
			?>
        <table id="example1" border="1" cellspacing="0" cellpadding="2" class="table table-bordered table-striped" align="center" width="80%">
            <tr>
                <th>Name of users</th>
                <th>City</th>
                <th>Current balance</th>
                <th>Commission</th>
                <th>Online</th>
            </tr>
            <?php
            	if(isset($_POST["filter"]))
                {
                    $sql = "SELECT * FROM `users` WHERE usertype=1 AND user_id IN(SELECT retailerID FROM brokerwise_retailer WHERE brokerID=".$_SESSION['user_id'].") ORDER BY ".$_POST["filter"];    
                }
                else
                    $sql = "SELECT * FROM `users` WHERE usertype=1 AND user_id IN(SELECT retailerID FROM brokerwise_retailer WHERE brokerID=".$_SESSION['user_id'].") ORDER BY username";
                $result = mysql_query($sql);
                while($row = mysql_fetch_array($result)) {
                    echo "<tr>
                    <td>".$row['username']."</td>
                    <td>".$row['city']."</td>
                    <td align='right'>".number_format($row['current_balance'],2,".","")."</td>
                    <td align='right'>".$row['user_commission']." </td>";
                    $datetime1 = strtotime($row['online']);
										$datetime2 = strtotime(date("Y-m-d H:i:s"));
										$interval  = abs($datetime2 - $datetime1);
										$minutes   = round($interval / 60);
                    if($minutes >= 15)
                        echo "<td align='center'>No</td>";
                    else
                        echo "<td align='center'>Yes</td>";
                    
                    echo "</tr>";
                }
            ?>
        </table>
    </body>
</html>
<script language="javascript">
	function confirmtoDelete(cid){
		var r = confirm("Confirm to delete this retailer?");
		if (r == true) {
		    location.replace("summaryOfBalance.php?act=delete&user_id="+cid);
		}
	}
	</script>
